export const config = {
  DB_SERVER_HOST: "localhost",
  DB_SERVER: "swagger_express_sequelize_node_mysql",
  DB_USER: "root",
  DB_PASSWORD: "root",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};

export const dialect = "mysql";
