import { Model, Table, Column, DataType } from "sequelize-typescript";

@Table({
  tableName: "users",
  timestamps: true, // Enables createdAt & updatedAt
})
export default class User extends Model {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    field: "id",
  })
  id?: number;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
    unique: true,
    field: "user_name",
  })
  userName!: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
    field: "first_name",
  })
  firstName!: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
    field: "last_name",
  })
  lastName!: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
    unique: true,
    field: "email",
  })
  email!: string;

  @Column({
    type: DataType.STRING(20),
    allowNull: true,
    field: "contact",
  })
  contact?: string;

  @Column({
    type: DataType.STRING(255),
    allowNull: false,
    field: "password",
  })
  password!: string;
}
