import express, { Application } from "express";
import cors, { CorsOptions } from "cors";
import Routes from "./routes";
import Database from "./config/sequelize.config";
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';

export default class Server {
  constructor(server: Application) {
    this.config(server);
    this.syncDatabase();
    new Routes(server);
    this.setupSwagger(server);
  }

  private config(server: Application): void {
    const corsOptions: CorsOptions = {
      origin: "http://localhost:5050"
    };

    server.use(cors(corsOptions));
    server.use(express.json());
    server.use(express.urlencoded({ extended: true }));
  }

  private syncDatabase(): void {
    const database = new Database();
    database.sequelize?.sync();
  }

  private setupSwagger(server: Application): void {
    const options = {
      swaggerDefinition: {
        openapi: '3.0.0',
        info: {
          title: 'Swagger Express Sequelize Node MySQL',
          version: '1.0.0',
          description: 'Description of the API',
        },
      },
      apis: ['./src/routes/*.ts'],
    };

    const specs = swaggerJSDoc(options);
    server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
  }
}
