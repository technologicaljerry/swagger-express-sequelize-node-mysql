import { Op } from "sequelize";
import User from "../models/user.model";

interface IUserRepository {
  save(user: User): Promise<User>;
  retrieveAll(searchParams: { userName?: string }): Promise<User[]>;
  retrieveById(userId: number): Promise<User | null>;
  update(user: User): Promise<number>;
  delete(userId: number): Promise<number>;
  deleteAll(): Promise<number>;
}

interface SearchCondition {
  [key: string]: any;
}

class UserRepository implements IUserRepository {
  async save(user: User): Promise<User> {
    try {
      return await User.create({
        userName: user.userName,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        contact: user.contact,
        password: user.password, // Ensure password is hashed before passing
      });
    } catch (err: any) {
      throw new Error(`Failed to create User: ${err.message}`);
    }
  }

  async retrieveAll(searchParams: { userName?: string }): Promise<User[]> {
    try {
      let condition: SearchCondition = {};
      if (searchParams?.userName) {
        condition.userName = { [Op.like]: `%${searchParams.userName}%` };
      }
      return await User.findAll({ where: condition });
    } catch (error: any) {
      throw new Error(`Failed to retrieve Users: ${error.message}`);
    }
  }

  async retrieveById(userId: number): Promise<User | null> {
    try {
      return await User.findByPk(userId);
    } catch (error: any) {
      throw new Error(`Failed to retrieve User: ${error.message}`);
    }
  }

  async update(user: User): Promise<number> {
    const { id, userName, firstName, lastName, email, contact, password } = user;
    try {
      const [affectedRows] = await User.update(
        { userName, firstName, lastName, email, contact, password },
        { where: { id }, returning: true }
      );
      return affectedRows;
    } catch (error: any) {
      throw new Error(`Failed to update User: ${error.message}`);
    }
  }

  async delete(userId: number): Promise<number> {
    try {
      return await User.destroy({ where: { id: userId } });
    } catch (error: any) {
      throw new Error(`Failed to delete User: ${error.message}`);
    }
  }

  async deleteAll(): Promise<number> {
    try {
      return await User.destroy({ where: {}, truncate: false });
    } catch (error: any) {
      throw new Error(`Failed to delete Users: ${error.message}`);
    }
  }
}

export default new UserRepository();
